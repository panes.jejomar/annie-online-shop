//dependencies and modules
let bcrypt = require('bcrypt');
let User = require('../models/User');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();
const JWT_KEY = `$process.env.JWT_SECRET_KEY`;

//authentication
module.exports.loginUser = (data) => {
    let userEmail = data.email;
    let userPassword = data.password;

    return User.findOne({email: userEmail}).then((foundRegisteredUser) => {
        if(foundRegisteredUser) {
            let userHashPW = foundRegisteredUser.password;
            return bcrypt
                .compare(userPassword, userHashPW)
                .then((isMatch, err) => {
                    if(err) {
                        return err
                    } else if (!isMatch) {
                        return ({message: `Wrong password`})
                    } else {
                        return ({accessToken: this.createToken(foundRegisteredUser)})
                    }
                })
        } else {
            return ({message: `Wrong credentials`})
        }

    })
};


//token creation
module.exports.createToken = (user) => {
    console.log(user)
    const data = {
        id: user.id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data, JWT_KEY, {})
};

//token verification
module.exports.verify = (req, res, next) => {
    console.log(req.headers.authorization);
    let token = req.headers.authorization;

    if(typeof token === "undefined") {
        return res.send({message: `Failed. No token`})
    } else{
        token = token.slice(7, token.length);
        jwt.verify(token, JWT_KEY, (error, decodedToken) => {
            if(error){
                return res.send({
                    auth: "Failed",
                    message: "Error"
            })
            } else{
                req.user = decodedToken;
                next();
            }
        })
    }

}

//token: if admin
module.exports.verifyAdmin = (req, res, next) => {
    if(req.user.isAdmin){
        next();
    } else{
        return res.send({
            auth: "Failed",
            message: "Action Forbidden"
        })
    }
}