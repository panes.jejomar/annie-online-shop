//dependencies and modules
let Product = require('../models/Product');
let User = require('../models/User');

//create a product
module.exports.createProduct = (data) => {
    let productName = data.name;
    let productPrice = data.price;
    let productDesc = data.description;

    let newProduct = new Product({
        pName: productName,
        pDescription: productDesc,
        pPrice: productPrice
    });
    return newProduct.save().then((savedProduct, err) => {
        if(err){
            return ({message: `Error in creating a new Product`});
        } else{
            return ({message: `Success in saving the ${productName}`})
        }
    })
};

//retrieve all products
module.exports.allProducts = (req, res) => {
    return Product.find({ }).then(outcome => {
        return res.send(outcome)
    })
};

//retrieve all ACTIVE products
module.exports.allActiveProducts = (req, res) => {
    return Product.find({pIsActive: true}).then(outcome => {
        return res.send(outcome)
    })
};

//retrieve product details
module.exports.getProductDetails = ((req, res) => {
    let id = req.params.id
    return Product.findById(id).then(foundProduct => {
        return res.send(foundProduct)
    }) 
});


//archive a product
module.exports.archiveProduct = ((req, res) => {
    let id = req.params.id;
    let archive = {
        pIsActive: false
    }
    return Product.findByIdAndUpdate(id, archive).then((archived, err) => {
        if(archived){
            return res.send({message: `Product ${id} deactivated`})
        } else{
            return res.send({message: `Failed to archive Product ${id}`})
        }
    })
})

//activate a product
module.exports.activateProduct = ((req, res) => {
    let id = req.params.id;
    let activate = {
        pIsActive: true
    }
    return Product.findByIdAndUpdate(id, activate).then((activated, err) => {
        if(activated){
            return res.send({message: `Product ${id} reactivated`})
        } else{
            return res.send({message: `Failed to reactivate Product ${id}`})
        }
    })
})

//update a product
module.exports.updateProduct = ((req, res) => {
    let data = req.body;
    let id = req.params.id;
     let update = {
        pName: data.name,
        pPrice: data.price,
        pDescription: data.description
     }
     return Product.findByIdAndUpdate(id, update).then((updated, err) => {
        if(updated){
            return res.send({ message: `Product ${id} updated`})
        } else{
            return res.send({ message: `Failed to update`})
        }
     })
})