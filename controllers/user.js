//dependencies and modules
let User = require('../models/User');
let bcrypt = require('bcrypt');
let dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
const JWT_KEY = `${process.env.JWT_SECRET_KEY}`

let salt = Number(process.env.SALT);


//REGISTER
module.exports.signUp = (data) => {
    let userName = data.name;
    let userEmail = data.email;
    let userPassword = data.password;
    let userContact = data.contact;
    let userAddress = data.address;

    let userRegister = new User({
        name: userName,
        email: userEmail,
        password: bcrypt.hashSync(userPassword, salt),
        contact: userContact,
        address: userAddress
    })

    return User.findOne({email: userEmail}).then(foundExistingUser => {
        if (foundExistingUser){
            return ({message: `Email already used`})
        } else {
            return userRegister.save().then((savedUser, saveError) => {
                if(saveError){
                    return ({message: `Error in registration`})
                } else{
                    return true;
                }
            })
        }
    })
};

//retrieve user details
module.exports.getUserDetails = (req, res) => {
    return User.findById(req.user.id).then(result => {
        return res.send(result)
    }).catch(err => {
        return res.send({message: `Error`})
    })
}
