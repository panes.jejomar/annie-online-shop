//dependencies and modules
const express = require('express');
const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const { findById } = require('../models/Product');

module.exports.createOrder = async(req, res) => {
    let userId = req.user.id;
    let orderedProductId = req.body.id;
    let orderedProductQty = req.body.quantity;

    //find the product
    let findProduct = await Product.findById(orderedProductId).then((foundProduct) => {
        return foundProduct
    })
    .catch((err) => ({message: `Error`}))

    let newOrder = new Order({
        totalAmount: (findProduct.pPrice * orderedProductQty),
        orderedBy: userId,
        productsOrdered: [{
            productId: orderedProductId,
            productQuantity: orderedProductQty
        }]
    })


    let orderCreatedResult = await newOrder.save().then((orderCreated) => {
        return orderCreated;
    })
    if(orderCreatedResult !== null){
        let updateUser = {
            orders: {
                orderId: orderCreatedResult._id
            }
        }
    

    return User.findByIdAndUpdate(userId, {$push : updateUser}).then((updated, err) => {
        if(updated){
            return res.send({message: `Succesfully place an order`})
        } else{
            return res.send({message: `Error in placing an order`})
        }
    })
    .catch((err) => ({message: `Error`}))

};

}

