//packages and dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");
const userRoute = require('./routes/user');
const productRoute = require('./routes/product');
const orderRoute = require('./routes/order');


//server setup
const app = express();
dotenv.config();
app.use(express.json());
const port = process.env.PORT;
const secret = process.env.CONNECTION_STRING;
app.use(cors());    

//application routes
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute)

//database connection
mongoose.connect(secret);
let dbStatus = mongoose.connection;
dbStatus.on('open', () => {
    console.log(`database is connected`)
})

//gateway response
app.get('/', (req, res) => {
    res.send(`Welcome to Annie's Online Shop`)
});

app.listen(port, () => {
    console.log(`server is running on ${port}`)
})