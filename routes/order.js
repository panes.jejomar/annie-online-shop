//dependencies and modules
let express = require('express');
const { verify, verifyAdmin } = require('../controllers/login');
let controller = require('../controllers/order');

//routing component
const route = express.Router();

//create an order
route.post('/create', verify, controller.createOrder)




//exports
module.exports = route;