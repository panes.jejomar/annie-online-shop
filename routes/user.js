//dependencies and modules
let express = require('express');
let controller = require('../controllers/user');
const { verify, verifyAdmin, loginUser } = require('../controllers/login')




//routing component
const route = express.Router();

//registration
route.post('/register', (req, res) => {
    let data = req.body;

    controller.signUp(data).then(result => {
        res.send(result)
    })
});

//login
route.post('/login', (req, res) => {
    let data = req.body;

    loginUser(data).then(result => {
        res.send(result)
    })
});

//get user details
route.get('/get-user', verify, controller.getUserDetails)





//export
module.exports = route;