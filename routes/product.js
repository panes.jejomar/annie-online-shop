//dependencies and modules
let express = require('express');
let controller = require('../controllers/product');
const { verify, verifyAdmin } = require('../controllers/login')

//routing component
let route = express.Router();

//create new product
route.post('/create', verify, verifyAdmin, (req, res) => {
    let data = req.body;

    controller.createProduct(data).then(result => {
        res.send(result)
    })
}); 

//get all active products
route.get('/', controller.allActiveProducts)

//get all products
route.get('/allproducts', verify, verifyAdmin, controller.allProducts)

//get product details
route.get('/:id/product', controller.getProductDetails)

//archive product
route.put('/:id/archive', verify, verifyAdmin,  controller.archiveProduct)

//activate product
route.put('/:id/activate', verify, verifyAdmin, controller.activateProduct)

//update product
route.put('/:id/update', verify, verifyAdmin, controller.updateProduct)

//export
module.exports = route;