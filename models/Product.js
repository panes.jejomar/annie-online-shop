//dependencies and modules
const mongoose = require('mongoose');

// blueprint.schema
const productSchema = new mongoose.Schema({
    pName: {
        type: String,
        required: [true, 'Product name is required']
    },
    pDescription: {
        type: String,
        require: [true, 'Product description is required']
    },
    pPrice: {
        type: Number,
        required: [true, 'Product price is required']
    },
    pIsActive: {
        type: Boolean,
        default: true
    },
    pCreatedOn: {
        type: Date,
        default: new Date()
    }
})

//export
const Product = mongoose.model('Product', productSchema);
module.exports = Product;