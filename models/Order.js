//dependencies and modules
const mongoose = require('mongoose');

//blueprint schema
let orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number
    },
    orderedOn: {
        type: Date,
        default: Date()
    },
    orderedBy: {
        type: String,
        required: [true, 'User required']
    },
    productsOrdered: [{
        productId: {
            type: String,
            required: [true, 'Product id is required']
        },
        productQuantity: {
            type: Number,
            default: 1
        }
    }]
});

//exports
const Order = mongoose.model('Order', orderSchema);
module.exports = Order;