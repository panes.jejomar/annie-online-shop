//dependencies and modules
const mongoose = require('mongoose');
const Product = require('./Product');

//blueprint schema
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Username is required']
    },
    email: {
        type: String,
        required: [true, 'Please input email address']
    },
    password: {
        type: String,
        required: [true, 'Please input password']
    },
    contact: {
        type: String,
        required: [true, 'Contact number is required']
    },
    address: {
        type: String,
        required: [true, 'Please input your delivery address']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: [
        {
        orderId: {
            type: String
            }
        }
    ]
});
//export
let User = mongoose.model('User', userSchema);
module.exports = User;
